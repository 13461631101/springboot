package com.example.springboot2.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.springboot2.entity.User;
import com.example.springboot2.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService extends ServiceImpl<UserMapper,User> {

    public boolean saveUser(User user){
        boolean update = saveOrUpdate(user);
        return update;

    }
//    @Autowired
//    private UserMapper userMapper;
//    public int save(User user){
//        if (user.getId()==null){//没有id表示 是新增
//            return userMapper.insert(user);
//        }else {//否则为更新
//            return userMapper.update(user);
//        }
//    }

}
