package com.example.springboot2.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.springboot2.entity.User;
import com.example.springboot2.mapper.UserMapper;
import com.example.springboot2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

//    @Autowired
//    private UserMapper userMapper;
    @Autowired
    private UserService userService;

//查询操作
    @GetMapping
    public List<User> index(){
//
//        List<User> all = userMapper.findAll();
//        return all;
        return userService.list();
    }
//    x新增或者更新操作
    @PostMapping
    public boolean save(@RequestBody User user){ //@RequestBody User user 将页面传过来的json转化为java对象
        return userService.saveUser(user);
//        return userMapper.insert(user);
    }
//地址是/user/id
    @DeleteMapping("/{id}")
    public boolean delete(@PathVariable Integer id){
//        return userMapper.deleteById(id);
        return userService.removeById(id);
    }


//分页查询 @RequestParam 接受 ? pageNum =1 & pageSize = 10
//    @GetMapping("/page")
//    public Map<String, Object> findPage(@RequestParam Integer pageNum, @RequestParam Integer pageSize,@RequestParam String username){
//        pageNum = (pageNum - 1) * pageSize;
//        username = "%" + username + "%";
//        List<User> data = userMapper.selectPage(pageNum, pageSize,username);
//        Integer total = userMapper.selectTotal(username);
//        //封装一下总条数和分页查询
//        Map<String, Object> res = new HashMap<>();
//        res.put("data",data);
//        res.put("total",total);
//        return res;
//    }

    @GetMapping("/page")
    public IPage<User> findPage(@RequestParam Integer pageNum, @RequestParam Integer pageSize,@RequestParam(defaultValue = "") String username,@RequestParam(defaultValue = "") String nickname,@RequestParam(defaultValue = "") String address){
        IPage<User> page = new Page<>(pageNum,pageSize);
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        if (!"".equals(username)){
            queryWrapper.like("username",username);
        }
        if (!"".equals(nickname)){
            queryWrapper.like("nickname",nickname);
        }
        if (!"".equals(address)){
            queryWrapper.like("address",address);
        }

        return  userService.page(page, queryWrapper);


    }

}
